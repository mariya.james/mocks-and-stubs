package printer;

/**
 *  DO NOT CHANGE THIS CLASS
 */
public class Printer {

    public void print(String printThis) {
        for (int i = 0; i < printThis.length(); i++) {
            try {
                Thread.sleep(1000);
                System.out.print(printThis.charAt(i));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
