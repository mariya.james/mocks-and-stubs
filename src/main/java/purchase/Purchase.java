package purchase;

import java.util.List;

/**
 *  DO NOT CHANGE THIS CLASS
 */
public class Purchase {

    private List<Item> items;

    public Purchase(List<Item> items) {
        this.items = items;
    }

    public String asString() {
        String out = "";

        for (Item item : items) {
            out += item.getName() + "\t" + item.getPrice() + "\n";
        }

        return out;
    }
}
