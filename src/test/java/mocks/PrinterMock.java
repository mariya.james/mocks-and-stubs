package mocks;

import printer.Printer;

public class PrinterMock extends Printer {

    private boolean verifyIfPrintIsCalled;
    private String verifyStringToBePrinted;

    public PrinterMock() {
        this.verifyIfPrintIsCalled = false;
    }

    @Override
    public void print(String printThis) {
        verifyIfPrintIsCalled = true;
        verifyStringToBePrinted=printThis;
    }

    public boolean verifyIfPrintIsCalled(){
        return verifyIfPrintIsCalled;
    }

    public String verifyPassedString(){
        return verifyStringToBePrinted;
    }
}
