package mocks;

import purchase.Item;
import purchase.Purchase;

import java.util.List;

public class PurchaseStub extends Purchase {
    public PurchaseStub(List<Item> items) {
        super(items);
    }

    @Override
    public String asString() {
        return "asString method is called...";
    }
}
