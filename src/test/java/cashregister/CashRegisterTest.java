package cashregister;

import mocks.PurchaseStub;
import org.junit.jupiter.api.Test;
import mocks.PrinterMock;
import org.mockito.Mockito;
import printer.Printer;
import purchase.Item;
import purchase.Purchase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CashRegisterTest {

    @Test
    void shouldPrintEmptyPurchase() {
        List<Item> items = new ArrayList<>();
        PrinterMock printerMock = new PrinterMock();
        CashRegister cashRegister = new CashRegister(printerMock);

        cashRegister.process(new Purchase(items));

        assertTrue(printerMock.verifyIfPrintIsCalled());

    }

    @Test
    void shouldPrintPurchaseWhenOneItemIsSend() {
        List<Item> items = new ArrayList<>();
        PrinterMock printerMock = new PrinterMock();
        CashRegister cashRegister = new CashRegister(printerMock);
        PurchaseStub purchaseStub = new PurchaseStub(items);

        cashRegister.process(purchaseStub);

        assertTrue(printerMock.verifyIfPrintIsCalled());
        assertEquals("asString method is called...",printerMock.verifyPassedString());

    }

    @Test
    void shouldPrintPurchaseListUsingMockito() {
        List<Item> items = new ArrayList<>();
        Printer printerMock = mock(Printer.class);
        CashRegister cashRegister = new CashRegister(printerMock);

        cashRegister.process(new Purchase(items));

        verify(printerMock).print("");
    }
}