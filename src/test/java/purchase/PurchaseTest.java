package purchase;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;

class PurchaseTest {

    @Test
    void asStringShouldReturnStringWithNoItems() {
        final Purchase purchase = new Purchase(emptyList());
        assertEquals("", purchase.asString());
    }

    @Test
    void asStringShouldReturnStringWithSingleItem() {
        final Item item1 = new Item("Cookie", 1);
        final Purchase purchase = new Purchase(Arrays.asList(item1));
        assertEquals("Cookie\t1.0\n", purchase.asString());
    }

    @Test
    void asStringShouldReturnStringWithMultipleItems() {
        final Item item1 = new Item("Cookie", 1);
        final Item item2 = new Item("Rectangle", 2);
        final Purchase purchase = new Purchase(Arrays.asList(item1, item2));
        assertEquals("Cookie\t1.0\nRectangle\t2.0\n", purchase.asString());
    }


}